## Start server

```
cd quick-carbon-server
npm install
npm start
```

By default, the server will start on **port 4000** defined in **.env.defaults**. Create your own .env file to override the default settings

```
cp .env.defaults .env
```

... and adjust accordingly.

Note: **make sure mongodb server is running locally**. The default database is "quick-carbon", and may be overriden inside the .env file.

## API Examples

See here for a full [list of API examples](./API.md).<br/>
Also check the [samples file](./SAMPLES.txt) for creating sample data.

## Remarks

Schemas can be found under src/models. They are mostly modeled after the [database structure](https://gitlab.com/our-sci/quick-carbon-pwa/blob/master/src/data/db.js) of the [quick-carbon-pwa](https://gitlab.com/our-sci/quick-carbon-pwa) project. Where applicable, "handles" are replaced by "custom string \_id's".

By default, mongodb uses a unique object id as the "primary key" named **\_id** (an example ObjectID is "5d7f6e70e623ab27ff8b6ae1" for instance). If one omits the **\_id** field when inserting a document, mongodb will **create an ObjectID automatically**. However, it is also perfectly viable to **use a custom unique string** instead of an ObjectID, especially for relatively smaller collections.

The [soildepth model](./src/models/soilDepth.js) uses a string (_'0_TO_10_CM', '10_TO_20_CM', etc._) instead of an ObjectID. Then, the [sample model](./src/models/sample.js) will have a human readable reference as the **soil_depth** field:

<pre>
$ mongo
use quick-carbon
> db.soildepths.findOne({_id: "10_TO_20_CM"})
{
	"_id" : "10_TO_20_CM",
	"name" : "10 - 20 cm",
	"min_depth" : 10,
	"max_depth" : 20,
	"units" : "CM",
	"__v" : 0
}
> db.samples.findOne()
{
  "_id" : ObjectId("5d7f6e70e623ab27ff8b6ae1"),
  <b>"soil_depth" : "10_TO_20_CM"</b>,
  "sample_id" : "bsdf123",
  "timestamp" : ISODate("2019-01-19T13:43:21.898Z"),
  "__v" : 0
}
</pre>

Note how **"soil_depth" = "10_TO_20_CM"** is **much more readable** than something like "soil_depth" = "5d7f6e70e623ab27jk39lls3" for instance.

The [organization model](./src/models/organization.js) also uses a string as the **primary key \_id**. This **\_id** field also **serves as a "handle"**.

Currently there is still **a lot of duplicate code** especially inside src/routes. One could probably create a helper function to **build standardized API routes for GET/POST/PUT**. However, with the current more verbose setup it is also easier to inject custom business logic inside specific routes if needed.

## Install log

See [here](./INSTALL_LOG.md)
