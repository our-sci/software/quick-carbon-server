import dotenv from "dotenv-defaults";
dotenv.config();

import express from "express";
import cors from "cors";

import { connectDb } from "./models";
import api from "./api";

const PATH_PREFIX = process.env.PATH_PREFIX;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get(`${PATH_PREFIX}/`, (req, res) => {
  return res.send("Hello there! This is the Quick Carbon Server.");
});

app.get("/", (req, res) => {
  return res.send(
    `Hello there! This is the Quick Carbon Server. <br/>
    Note that request paths are expected to start with ${PATH_PREFIX}`
  );
});

// routes
app.use(`${PATH_PREFIX}/api`, api);

// fallback not found
app.use((req, res) => {
  return res.status(404).send({
    message: "What you are looking for does not exist.",
    path: req.path,
    status: 404
  });
});

connectDb().then(async () => {
  app.listen(process.env.PORT, () => {
    console.log(`Express app listening on ${process.env.PORT}`);
  });
});
