import { Router } from "express";
import routes from "../routes";

const router = Router();

router.use("/organization", routes.organization);
router.use("/field", routes.field);
router.use("/samplingevent", routes.samplingEvent);
router.use("/sample", routes.sample);
router.use("/soildepth", routes.soilDepth);
router.use("/locationcollection", routes.locationCollection);

export default router;
