import mongoose from "mongoose";

import Field from "./field";
import Organization from "./organization";
import Sample from "./sample";
import SamplingEvent from "./samplingEvent";
import SoilDepth from "./soilDepth";
import LocationCollection from './locationCollection';

const connectDb = () => {
  return mongoose.connect(process.env.DATABASE_URL, {
    useFindAndModify: false
  });
};

const models = {
  Field,
  Organization,
  Sample,
  SamplingEvent,
  SoilDepth,
  LocationCollection, 
};

export { connectDb };

export default models;
