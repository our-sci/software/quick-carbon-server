import mongoose, { Schema } from "mongoose";

const organizationSchema = new Schema(
  {
    _id: { type: String },
    name: String
  },
  { getters: true }
);

// TODO: determine if 'handle' field is needed after all
/*
organizationSchema.virtual("handle").get(() => {
  return this._id;
});
*/

const Organization = mongoose.model("Organization", organizationSchema);

export default Organization;
