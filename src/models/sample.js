import mongoose, { Schema } from "mongoose";

// const sampleSchema = new Schema({
//   soil_depth: { type: String, ref: "SoilDepth" },
//   sample_id: String,
//   timestamp: Date
// });

const pointSchema = new Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

const sampleSchema = new Schema({
  _id: {
    type: String,
  },
  geometry: {
    type: pointSchema,
    required: true,
  },
  properties: {
    depthId: { 
      type: String, 
      ref: "SoilDepth" 
    },
    // TODO: fix LocationCollection to actually create a collection of Locations. 
    // Right now we're not actually using Location, LocationCollection just stores an array
    locationId: {
      type: Schema.Types.ObjectId,
      ref: "Location",
    },
    locationId: String,
    sampleId: String,
    timestamp: Date,
    // TODO: make this work
    surveyId: {
      type: Schema.Types.ObjectId,
      ref: "SamplingEvent",
    },

  },

});

const Sample = mongoose.model("Sample", sampleSchema);

export default Sample;
