import mongoose, { Schema } from "mongoose";

// TODO: need to clarify sampling_events relation
const fieldSchema = new Schema({
  _id: {
    // type: Schema.Types.ObjectId,
    type: String,
    required: true,
  },
  id: {
    type: String,
  },
  type: { 
    type: String,
    enum: ['Feature'],
    // required: true,
  },
  geometry: {
    coordinates: [[[Number]]],
    type: { type: String },
  },
  properties: {
    name: String,
    handle: String,
    // organization: 
    depths: [{
      // type: String,
      type: String,
      ref: "SoilDepth",
    }],
    hasAdditionalQuestions: Boolean,
    email: String,
    createdAt: Number,
    surveyLocations: {
      // type: Schema.Types.ObjectId,
      type: String,
      ref: "LocationCollection",
    },
    samplingEvents: [
      {
        type: Schema.Types.ObjectId,
        ref: "SamplingEvent"
      },
    ],

    
  }
});

const Field = mongoose.model("Field", fieldSchema);

export default Field;
