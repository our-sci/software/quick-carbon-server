import mongoose, { Schema } from "mongoose";

const soilDepthSchema = new Schema({
  _id: String,
  name: String,
  min_depth: Number,
  max_depth: Number,
  units: String
});

soilDepthSchema.virtual("handle").get(() => {
  return this._id;
});

const SoilDepth = mongoose.model("SoilDepth", soilDepthSchema);

export default SoilDepth;
