import mongoose, { Schema } from "mongoose";

// TODO: need to clarify samples relation
const samplingEventSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  samples: [{ type: Schema.Types.ObjectId, ref: "Sample" }],
  modifiedAt: Date,
  startedAt: Date,
  completedAt: Date,
});

const SamplingEvent = mongoose.model("SamplingEvent", samplingEventSchema);

export default SamplingEvent;
