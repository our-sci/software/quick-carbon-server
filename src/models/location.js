import mongoose, {
	Schema
} from "mongoose";

const pointSchema = new Schema({
	type: {
		type: String,
		enum: ['Point'],
		required: true,
	},
	coordinates: {
		type: [Number],
		required: true,
	},
});

const locationSchema = new Schema({
	type: {
		type: String,
		enum: ['Feature'],
		required: true,
	},
	geometry: {
		type: pointSchema,
		required: true,
	},
	properties: {
		// timestamp: Date,
		label: String,
		type: String,
	},
	_id: {
		type: Schema.Types.ObjectId,
		required: true,
	},
});

const Location = mongoose.model("Location", locationSchema);

export default Location;


// {
// 	type: 'Feature',
// 	id: 'XYbToXGH9W/tKZ05',
// 	properties: {
// 		label: '0',
// 		type: 'SURVEY_LOCATION_MARKER',
// 		id: 'XYbToXGH9W/tKZ05',
// 	},
// 	geometry: {
// 		type: 'Point',
// 		coordinates: [-120.65746161537712, 38.13002492488401],
// 	},