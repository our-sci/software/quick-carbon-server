import mongoose, {
	Schema
} from "mongoose";

const pointSchema = new Schema({
	type: {
		type: String,
		enum: ['Point'],
		required: true,
	},
	coordinates: {
		type: [Number],
		required: true,
	},
});

const locationSchema = new Schema({
	type: {
		type: String,
		enum: ['Feature'],
		required: true,
	},
	geometry: {
		type: pointSchema,
		required: true,
	},
	id: {
		type: String,
		// ref: "Location",
	},
	properties: {
		label: String,
		type: { type: String },
		id: String,
	},
	timestamp: Date,
});

// TODO: need to clarify samples relation
const locationCollectionSchema = new Schema({
	id: {
		type: String,
		required: true,
	},
	type: {
		type: String,
		enum: ['FeatureCollection'],
		required: true,
	},
	// features: [{
	// 	type: Schema.Types.ObjectId,
	// 	ref: "Location",
	// 	required: true,
	// }],
	features: [{
		type: locationSchema,
		required: true,
	}],
	properties: {
		
	},
	modified_at: Date,
	started_at: Date,
	completed_at: Date,
});

const LocationCollection = mongoose.model("LocationCollection", locationCollectionSchema);

export default LocationCollection;
