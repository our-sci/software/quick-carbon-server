import { Router } from "express";
import mongoose from "mongoose";
import models from "../models";

const router = Router();
const { SoilDepth } = models;

router.get("/", async (req, res) => {
  const entities = await SoilDepth.find();
  return res.send(entities);
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  const entity = await SoilDepth.findOne({ _id: id });
  if (!entity) {
    return res.status(404).send({
      message: `No entity exists with id=${id}`
    });
  }
  return res.send(entity);
});

router.post("/", async (req, res) => {
  const entity = req.body;
  try {
    const created = await SoilDepth.create(entity);
    return res.send(created);
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      return res.status(409).send({ message: "Entity already exists" });
    }
  }
  return res.status(500).send({ message: "Internal error" });
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;

  // find existing entity
  const existing = await SoilDepth.findOne({
    _id: id
  });

  if (!existing) {
    return res.status(404).send({
      message: `No entity exists with id=${id}`
    });
  }

  // update entity
  // Note: new=true option returns the new document
  // instead of the default unaltered document
  try {
    const updated = await SoilDepth.findOneAndUpdate({ _id: id }, req.body, {
      new: true
    });
    return res.send(updated);
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: "Ouch :/" });
  }
});

export default router;
