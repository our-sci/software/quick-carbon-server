import { Router } from "express";
import mongoose from "mongoose";
import models from "../models";

const router = Router();
const { Sample } = models;

router.get("/", async (req, res) => {
  let entities;
  if (req.query.populate) {
    entities = await Sample.find().populate(req.query.populate);
  } else {
    entities = await Sample.find();
  }

  return res.send(entities);
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  let entity;

  if (req.query.populate) {
    entity = await Sample.findOne({ _id: id }).populate(req.query.populate);
  } else {
    entity = await Sample.findOne({ _id: id });
  }

  if (!entity) {
    return res.status(404).send({
      message: `No entity exists with id=${id}`
    });
  }

  return res.send(entity);
});

router.post("/", async (req, res) => {
  const entity = req.body;
  console.log(entity);
  try {
    const created = await Sample.create(entity);
    return res.send(created);
  } catch (err) {
    if (err.name === "MongoError" && err.code === 11000) {
      return res.status(409).send({ message: "Entity already exists" });
    }
  }
  return res.status(500).send({ message: "Internal error" });
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;

  // find existing entity
  const existing = await Sample.findOne({
    _id: id
  });

  if (!existing) {
    return res.status(404).send({
      message: `No entity exists with id=${id}`
    });
  }

  // update entity
  // Note: new=true option returns the new document
  // instead of the default unaltered document
  try {
    const updated = await Sample.findOneAndUpdate({ _id: id }, req.body, {
      new: true
    });
    return res.send(updated);
  } catch (err) {
    console.log(err);
    return res.status(500).send({ message: "Ouch :/" });
  }
});

export default router;
