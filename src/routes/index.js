import debug from "./debug";

// TODO: create a helper function to create the routing code for the following entities
import organization from "./organization";
import field from "./field";
import samplingEvent from "./samplingEvent";
import sample from "./sample";
import soilDepth from "./soilDepth";
import locationCollection from './locationCollection';

export default {
  debug,
  organization,
  field,
  samplingEvent,
  sample,
  soilDepth,
  locationCollection,
};
