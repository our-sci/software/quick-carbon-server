#!/bin/bash

pm2 stop quick-carbon-server
git pull
npm install
pm2 start quick-carbon-server
