## Install log

Initial project setup mostly from this [node tutorial](https://www.robinwieruch.de/node-js-express-tutorial).

```
mkdir quick-carbon-server
cd quick-carbon-server
npm init -y
npm install nodemon --save-dev
npm install @babel/core @babel/node --save-dev
npm install @babel/preset-env --save-dev
touch .babelrc
```

> {<br/>
> "presets": ["@babel/preset-env"]<br/>
> }

```
curl https://gitignore.io/api/node > .gitignore
npm i dotenv-defaults --save

touch .env.defaults
```

> PORT=3001<br />
> DATABASE_URL=mongodb://localhost:27017/quick-carbon

```
touch .env
```

> PORT=_(your deployment)_<br/>
> DATABASE*URL=*(your deployment)\_

Note that **.env is not committed** to GIT source control.

```
npm install express
npm install cors
```

```
npm install mongoose --save
```
